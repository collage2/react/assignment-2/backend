import mongoose from "mongoose";

const Schema = mongoose.Schema;

const Product = new Schema({
    title: String,
    description: String,
    price: Number,
    imageUrl: String,
});

export default mongoose.model("Product", Product);
