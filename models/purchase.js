import mongoose from "mongoose";

const Schema = mongoose.Schema;

const Purchase = new Schema({
    products: [
        {
            title: String,
            description: String,
            price: Number,
            imageUrl: String,
        }
    ]
});

export default mongoose.model("Purchase", Purchase);
