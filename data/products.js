export default [
    {
        id: 1,
        title: 'Milk',
        price: 15,
        imageUrl: 'http://localhost:3000/images/milk.png',
        description: 'Regular cow milk',
    },
    {
        id: 2,
        title: 'Coffe',
        price: 20,
        imageUrl: 'http://localhost:3000/images/coffe.png',
        description: 'The best coffe',
    },
    {
        id: 3,
        title: 'Corn',
        price: 5,
        imageUrl: 'http://localhost:3000/images/corn.png',
        description: 'All you need is a good corn',
    },
    {
        id: 4,
        title: 'Bread',
        price: 40,
        imageUrl: 'http://localhost:3000/images/bread.png',
        description: 'Warm and delicios bread',
    },
    {
        id: 5,
        title: 'Eggs',
        price: 17,
        imageUrl: 'http://localhost:3000/images/eggs.png',
        description: 'Great and fresh eggs',
    },
    {
        id: 6,
        title: 'Wipes',
        price: 25,
        imageUrl: 'http://localhost:3000/images/wipes.png',
        description: 'Soft and clean wipes',
    },
]