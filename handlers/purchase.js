import Purchase from '../models/purchase.js';

export const createPurchase = async (products) => {
    const newPurchase = new Purchase({ products });
    await newPurchase.save();
}