import Product from '../models/product.js';
import products from '../data/products.js';

export const insertProducts = async () => {
    await Product.insertMany(products);
}

export const getProducts = async () => (
    await Product.find({})
)