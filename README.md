# Assignment 2 Backend
We did the project together on the same computer using the pair programing method.


## Getting started
```
docker run -d --name mongo -p 27017:27017 -v "$(PWD)/db-volume":/data/db mongo
npm install
npm start
```

## Authores
1. Itamar Smirra - 209173822
2. Jonathan Harmatz - 207017146
