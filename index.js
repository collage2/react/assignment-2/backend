import * as dotenv from 'dotenv';
dotenv.config();
import mongoose from 'mongoose';
import express from 'express';
import cors from 'cors';
import router from './routes/router.js';
import { insertProducts } from './handlers/products.js';

const app = express();
app.use(express.json());
app.use(express.static('public'));
app.use(cors());
app.use(router);

app.listen(3000, async () => {
    await mongoose.connect(process.env.MONGO_URL);
    await insertProducts();
    console.log(`Listening on port 3000!`);
});