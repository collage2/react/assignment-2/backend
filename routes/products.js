import express from "express";
import { getProducts } from '../handlers/products.js';
import { createPurchase } from '../handlers/purchase.js';

const router = express.Router();

router.get('/', async (req, res) => {
    const products = await getProducts();
    res.send(products);
});

router.post('/buy', async (req, res) => {
    await createPurchase(req.body);
    res.send();
});

export default router;