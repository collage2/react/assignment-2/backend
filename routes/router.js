import express from "express";
import productsRouter from './products.js';

const router = express.Router();

router.get('/', (req, res) => {
    res.send('Hello World');
});

router.use('/products', productsRouter);

export default router;